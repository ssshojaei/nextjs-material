import { jssPreset, StylesProvider } from '@material-ui/core/styles'
import { create } from 'jss'
import rtl from 'jss-rtl'

const jss = create({ plugins: [...jssPreset().plugins, rtl()] })

export default ({ children }) => (
  <StylesProvider jss={jss}>{children}</StylesProvider>
)
