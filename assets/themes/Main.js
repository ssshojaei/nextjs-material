import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  direction: 'rtl',
  palette: {
    primary: {
      main: '#000080',
    },
    greenBlue: {
      main: '#00CED1',
    },
    secondary: {
      main: '#4E4B66',
    },
    error: {
      main: '#DB4760',
    },
    warning: {
      main: '#FFBC23',
    },
    background: {
      default: '#fff',
    },
    success: {
      main: '#00BA88',
    },
  },
})

export default theme
