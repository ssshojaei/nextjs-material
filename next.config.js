/** @format */

module.exports = {
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.jsx?$/,
      },
      use: ["@svgr/webpack", "url-loader"],
    });

    return config;
  },
};
