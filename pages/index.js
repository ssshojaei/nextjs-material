import { Box, Button, Container, Grid } from '@material-ui/core'

const HomePage = () => {
  return (
    <Grid container>
      <Grid item xs={12}>
        <Container>
          <Box marginTop={10} textAlign="center">
            <Button variant="contained" color="primary">
              Primary
            </Button>
          </Box>
        </Container>
      </Grid>
    </Grid>
  )
}

export default HomePage
